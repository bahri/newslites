import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:newslite/Themes/fiturthemes.dart';
import 'package:newslite/Themes/imagepath.dart';
import 'package:newslite/views/home.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(milliseconds: 3500),
        () => Get.off(() => const HomePage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: blackColor,
      body: ListView(
        children: [
          const SizedBox(
            height: 200,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 18,
                  ),
                  Container(
                    height: 200,
                    width: 200,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      color: Colors.white,
                      image: DecorationImage(
                        image: AssetImage("assets/images/news.jpg"),
                      ),
                    ),
                  ),
                ],
              ),
              IntrinsicHeight(
                child: new Row(
                  children: <Widget>[
                    VerticalDivider(
                      color: goldColor,
                      thickness: 2,
                      width: 30,
                    ),
                    Text(
                      'News\nLite',
                      style: skyBlueStyle.copyWith(
                        fontSize: 25,
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
