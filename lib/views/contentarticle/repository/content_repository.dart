import 'package:newslite/views/contentarticle/controller/content_controller.dart';
import 'package:newslite/views/contentarticle/models/content_model.dart';

class ApiRepositoryContentArticle {
  final _providerContentArticle = ApiProviderContentArticle();

  Future<ContentArticleModels> fetchContentArticleList() {
    return _providerContentArticle.fetchContentArticleList();
  }
}

class NetworkErrorNewArticle extends Error {}
