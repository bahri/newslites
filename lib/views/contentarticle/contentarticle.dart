part of 'fiturcontentarticle.dart';

class ContentArticle extends StatefulWidget {
  // ignore: prefer_typing_uninitialized_variables
  final titles, desc, publish, author, image, categories;
  const ContentArticle(
      {Key? key,
      this.titles,
      this.desc,
      this.publish,
      this.author,
      this.image,
      this.categories})
      : super(key: key);

  @override
  State<ContentArticle> createState() => _ContentArticleState();
}

class _ContentArticleState extends State<ContentArticle> {
  DateFormat dateFormat = DateFormat("dd MMMM yyyy");
  final ContentArticleBloc _contentArticleBloc = ContentArticleBloc();
  @override
  void initState() {
    _contentArticleBloc.add(GetContentArticleList());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Widget kontenArtikel() => Column(
    //       children: const [
    //         BorderNewArticle(),
    //       ],
    //     );

    Widget cardContentArticle(BuildContext context) => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  height: defaultMargin + 30,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: primaryColor,
                  ),
                ),
                ListTile(
                  leading: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(
                      Icons.close,
                      color: whiteColor,
                    ),
                  ),
                  title: Text(
                    "${widget.categories}" == ""
                        ? "-"
                        : "${widget.categories}".toUpperCase(),
                    style: whiteTextStyle.copyWith(fontWeight: bold),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 20),
              child: Text(
                  "${widget.titles}" == ""
                      ? "-"
                      : "${widget.titles}".toUpperCase(),
                  style:
                      blackTextStyle.copyWith(fontWeight: bold, fontSize: 20)),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Container(
                    height: defaultMargin + 8,
                    width: defaultMargin + 8,
                    decoration: BoxDecoration(
                      color: whiteColor.withOpacity(0.3),
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: blackColor,
                        width: 2,
                      ),
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 20),
                      child: Text("${widget.author}",
                          textWidthBasis: TextWidthBasis.parent,
                          textAlign: TextAlign.justify,
                          style: blackTextStyle.copyWith(
                              fontWeight: semiBold, fontSize: 12)),
                    ),
                    const SizedBox(
                      height: 3,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, right: 20),
                      child: Text(
                          dateFormat.format(DateTime.parse(
                              widget.publish.toString() == ""
                                  ? "-"
                                  : widget.publish.toString())),
                          style: blackTextStyle.copyWith(fontWeight: light)),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 225,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                // borderRadius: const BorderRadius.all(Radius.circular(15)),
                image: DecorationImage(
                  image: NetworkImage(
                    "${widget.image}" == "" ? "-" : "${widget.image}",
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 12),
              child: HtmlWidget(
                "${widget.desc}",
                textStyle: graysLatoTextStyle.copyWith(
                    fontWeight: regular,
                    height: 1.7,
                    letterSpacing: 2,
                    fontSize: 16),
              ),
              // child: Text(
              //   "${widget.desc}" == "" ? "-" : "${widget.desc}",
              //   // textAlign: TextAlign.justify,
              //   style: graysLatoTextStyle.copyWith(
              //       fontWeight: regular,
              //       height: 1.7,
              //       letterSpacing: 2,
              //       fontSize: 16),
              // ),
            ),
            const SizedBox(
              height: 40,
            ),
          ],
        );

    Widget buildListContentArticle() => BlocProvider(
          create: (_) => _contentArticleBloc,
          child: BlocListener<ContentArticleBloc, ContentArticleState>(
            listener: (context, state) {
              if (state is ContentArticleError) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(state.message!),
                  ),
                );
              }
            },
            child: BlocBuilder<ContentArticleBloc, ContentArticleState>(
              builder: (context, state) {
                if (state is ContentArticleInitial) {
                  return const Center(child: CircularProgressIndicator());
                } else if (state is ContentArticleLoading) {
                  return const Center(child: CircularProgressIndicator());
                } else if (state is ContentArticleLoaded) {
                  //Ini content
                  // return Container();
                  return cardContentArticle(context);
                } else if (state is ContentArticleError) {
                  return Container();
                } else {
                  return Container();
                }
              },
            ),
          ),
        );

    return Scaffold(
      backgroundColor: whiteColor,
      body: ListView(
        children: [
          buildListContentArticle(),
          // const SizedBox(
          //   height: 40,
          // ),
          // kontenArtikel(),
          const SizedBox(
            height: 40,
          ),
        ],
      ),
    );
  }
}
