// ignore_for_file: avoid_print

import 'package:dio/dio.dart';
import 'package:newslite/serviceAPI/inputapi.dart';
import 'package:newslite/views/contentarticle/models/content_model.dart';

class ApiProviderContentArticle {
  final Dio _dio = Dio();
  final String _url = InputAPI.ContentArticle;

  Future<ContentArticleModels> fetchContentArticleList() async {
    try {
      print("ContentArticle " + _url);

      Response response = await _dio.get(_url);
      print("ContentArticle " "${response.data}");
      return ContentArticleModels.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return ContentArticleModels.withError(
          "Data not found / Connection issue");
    }
  }
}
