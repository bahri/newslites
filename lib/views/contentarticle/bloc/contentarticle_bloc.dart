import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:newslite/views/contentarticle/models/content_model.dart';
import 'package:newslite/views/contentarticle/repository/content_repository.dart';

part 'contentarticle_event.dart';
part 'contentarticle_state.dart';

class ContentArticleBloc
    extends Bloc<ContentArticleEvent, ContentArticleState> {
  ContentArticleBloc() : super(ContentArticleInitial()) {
    final ApiRepositoryContentArticle _apiRositoryContentArticle =
        ApiRepositoryContentArticle();
    on<ContentArticleEvent>((event, emit) async {
      try {
        emit(ContentArticleLoading());
        final mListContentArticle =
            await _apiRositoryContentArticle.fetchContentArticleList();
        emit(ContentArticleLoaded(contentArticleModels: mListContentArticle));
        if (mListContentArticle.error != null) {
          emit(ContentArticleError(mListContentArticle.error));
        }
      } on NetworkErrorNewArticle {
        emit(const ContentArticleError(
            "Failled to fetch data, is your device online?"));
      }
    });
  }
}
