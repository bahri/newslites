part of 'contentarticle_bloc.dart';

abstract class ContentArticleEvent extends Equatable {
  const ContentArticleEvent();

  @override
  List<Object> get props => [];
}

class GetContentArticleList extends ContentArticleEvent {}
