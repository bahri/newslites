part of 'contentarticle_bloc.dart';

abstract class ContentArticleState extends Equatable {
  const ContentArticleState();

  @override
  List<Object> get props => [];
}

class ContentArticleInitial extends ContentArticleState {}

class ContentArticleLoading extends ContentArticleState {}

class ContentArticleLoaded extends ContentArticleState {
  final ContentArticleModels contentArticleModels;
  const ContentArticleLoaded({
    required this.contentArticleModels,
  });
}

class ContentArticleError extends ContentArticleState {
  final String? message;

  const ContentArticleError(this.message);
}
