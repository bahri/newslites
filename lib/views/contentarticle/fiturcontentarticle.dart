import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';

import 'package:intl/intl.dart';
import 'package:newslite/Themes/theme.dart';
import 'package:newslite/views/contentarticle/bloc/contentarticle_bloc.dart';

part 'contentarticle.dart';
