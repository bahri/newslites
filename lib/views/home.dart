import 'package:flutter/material.dart';

import 'package:newslite/Themes/fiturthemes.dart';
import 'package:newslite/views/artikel/fiturartikel.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: blackColor,
      appBar: AppBar(
        backgroundColor: blackColor,
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'News Lite',
              style: skyBlueStyle.copyWith(fontSize: 25),
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: Center(
          child: Article(),
        ),
      ),
    );
  }
}
