// ignore_for_file: prefer_void_to_null, unnecessary_question_mark, unnecessary_new, prefer_collection_literals, unnecessary_this
class ArticleModels {
  int? currentPage;
  List<Data>? data;
  String? firstPageUrl;
  int? from;
  int? lastPage;
  String? lastPageUrl;
  List<Links>? links;
  String? nextPageUrl;
  String? path;
  int? perPage;
  Null? prevPageUrl;
  int? to;
  int? total;
  String? error;
  ArticleModels(
      {this.currentPage,
      this.data,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.links,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});
  ArticleModels.withError(String errorMessage) {
    error = errorMessage;
  }
  ArticleModels.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    if (json['links'] != null) {
      links = <Links>[];
      json['links'].forEach((v) {
        links!.add(new Links.fromJson(v));
      });
    }
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    if (this.links != null) {
      data['links'] = this.links!.map((v) => v.toJson()).toList();
    }
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class Data {
  int? id;
  String? title;
  String? slug;
  Image? image;
  String? content;
  int? createdBy;
  String? publishAt;
  String? beautyPublishDate;
  List<Tag>? tag;
  List<Category>? category;
  User? user;

  Data(
      {this.id,
      this.title,
      this.slug,
      this.image,
      this.content,
      this.createdBy,
      this.publishAt,
      this.beautyPublishDate,
      this.tag,
      this.category,
      this.user});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    slug = json['slug'];
    image = json['image'] != null ? new Image.fromJson(json['image']) : null;
    content = json['content'];
    createdBy = json['created_by'];
    publishAt = json['publish_at'];
    beautyPublishDate = json['beauty_publish_date'];
    if (json['tag'] != null) {
      tag = <Tag>[];
      json['tag'].forEach((v) {
        tag!.add(new Tag.fromJson(v));
      });
    }
    if (json['category'] != null) {
      category = <Category>[];
      json['category'].forEach((v) {
        category!.add(new Category.fromJson(v));
      });
    }
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['slug'] = this.slug;
    if (this.image != null) {
      data['image'] = this.image!.toJson();
    }
    data['content'] = this.content;
    data['created_by'] = this.createdBy;
    data['publish_at'] = this.publishAt;
    data['beauty_publish_date'] = this.beautyPublishDate;
    if (this.tag != null) {
      data['tag'] = this.tag!.map((v) => v.toJson()).toList();
    }
    if (this.category != null) {
      data['category'] = this.category!.map((v) => v.toJson()).toList();
    }
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class Image {
  String? path;
  String? source;

  Image({this.path, this.source});

  Image.fromJson(Map<String, dynamic> json) {
    path = json['path'];
    source = json['source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['path'] = this.path;
    data['source'] = this.source;
    return data;
  }
}

class Tag {
  int? id;
  String? name;
  String? slug;
  Null? description;
  int? createdBy;
  String? createdAt;
  String? updatedAt;
  Null? deletedAt;
  Pivot? pivot;

  Tag(
      {this.id,
      this.name,
      this.slug,
      this.description,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.pivot});

  Tag.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    description = json['description'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['description'] = this.description;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.pivot != null) {
      data['pivot'] = this.pivot!.toJson();
    }
    return data;
  }
}

class Pivot {
  int? postId;
  int? postTagId;

  Pivot({this.postId, this.postTagId});

  Pivot.fromJson(Map<String, dynamic> json) {
    postId = json['post_id'];
    postTagId = json['post_tag_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this.postId;
    data['post_tag_id'] = this.postTagId;
    return data;
  }
}

class Category {
  int? id;
  String? name;
  String? slug;
  int? createdBy;
  String? createdAt;
  String? updatedAt;
  Null? deletedAt;
  Pivot? pivot;

  Category(
      {this.id,
      this.name,
      this.slug,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.pivot});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    createdBy = json['created_by'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['created_by'] = this.createdBy;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.pivot != null) {
      data['pivot'] = this.pivot!.toJson();
    }
    return data;
  }
}

class PivotCategory {
  int? postId;
  int? postCategoryId;

  PivotCategory({this.postId, this.postCategoryId});

  PivotCategory.fromJson(Map<String, dynamic> json) {
    postId = json['post_id'];
    postCategoryId = json['post_category_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this.postId;
    data['post_category_id'] = this.postCategoryId;
    return data;
  }
}

class User {
  int? id;
  int? roleId;
  String? name;
  String? email;
  String? emailVerifiedAt;
  Null? passwordMd5;
  String? status;
  Null? provider;
  Null? providerId;
  Null? providerAvatar;
  Null? originName;
  Null? originId;
  Null? sourceName;
  Null? sourceUrl;
  String? createdAt;
  String? updatedAt;
  Null? deletedAt;
  String? initial;
  String? nickName;
  Role? role;

  User(
      {this.id,
      this.roleId,
      this.name,
      this.email,
      this.emailVerifiedAt,
      this.passwordMd5,
      this.status,
      this.provider,
      this.providerId,
      this.providerAvatar,
      this.originName,
      this.originId,
      this.sourceName,
      this.sourceUrl,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.initial,
      this.nickName,
      this.role});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roleId = json['role_id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    passwordMd5 = json['password_md5'];
    status = json['status'];
    provider = json['provider'];
    providerId = json['provider_id'];
    providerAvatar = json['provider_avatar'];
    originName = json['origin_name'];
    originId = json['origin_id'];
    sourceName = json['source_name'];
    sourceUrl = json['source_url'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    initial = json['initial'];
    nickName = json['nick_name'];
    role = json['role'] != null ? new Role.fromJson(json['role']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['role_id'] = this.roleId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['password_md5'] = this.passwordMd5;
    data['status'] = this.status;
    data['provider'] = this.provider;
    data['provider_id'] = this.providerId;
    data['provider_avatar'] = this.providerAvatar;
    data['origin_name'] = this.originName;
    data['origin_id'] = this.originId;
    data['source_name'] = this.sourceName;
    data['source_url'] = this.sourceUrl;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['initial'] = this.initial;
    data['nick_name'] = this.nickName;
    if (this.role != null) {
      data['role'] = this.role!.toJson();
    }
    return data;
  }
}

class Role {
  int? id;
  String? name;
  String? createdAt;
  String? updatedAt;

  Role({this.id, this.name, this.createdAt, this.updatedAt});

  Role.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Links {
  String? url;
  String? label;
  bool? active;

  Links({this.url, this.label, this.active});

  Links.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    label = json['label'];
    active = json['active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = this.url;
    data['label'] = this.label;
    data['active'] = this.active;
    return data;
  }
}
