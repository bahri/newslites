// ignore_for_file: avoid_print

import 'package:dio/dio.dart';
import 'package:newslite/serviceAPI/inputapi.dart';
import 'package:newslite/views/artikel/models/article_models.dart';

class ApiProviderArticle {
  final Dio _dio = Dio();
  final String _url = InputAPI.ContentArticle;

  Future<ArticleModels> fetchArticleList() async {
    try {
      Response response = await _dio.get(_url);
      print("Content List " + _url);

      print("Content List " "${response.data}");
      return ArticleModels.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return ArticleModels.withError("Data not found / Connection issue");
    }
  }
}
