import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:newslite/views/artikel/models/article_models.dart';
import 'package:newslite/views/artikel/repository/apiarticle_repository.dart';

part 'article_event.dart';
part 'article_state.dart';

class ArticleBloc extends Bloc<ArticleEvent, ArticleState> {
  ArticleBloc() : super(ArticleInitial()) {
    final ApiRepositoryArticle _apiRepositoryArticle = ApiRepositoryArticle();
    on<ArticleEvent>((event, emit) async {
      try {
        emit(ArticleLoading());
        final mList = await _apiRepositoryArticle.fetchArticleList();
        emit(ArticleLoaded(articleModels: mList));
        if (mList.error != null) {
          emit(ArticleError(mList.error));
        }
      } on NetworkError {
        emit(const ArticleError(
            "Failled to fetch data, is your device online?"));
      }
    });
  }
}
