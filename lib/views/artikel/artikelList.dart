// ignore_for_file: avoid_types_as_parameter_names, non_constant_identifier_names, file_names

part of 'fiturartikel.dart';

class ArtikelList extends StatefulWidget {
  const ArtikelList({Key? key}) : super(key: key);

  @override
  State<ArtikelList> createState() => _ArtikelListState();
}

class _ArtikelListState extends State<ArtikelList> {
  final ArticleBloc _articleBloc = ArticleBloc();
  @override
  void initState() {
    _articleBloc.add(GetArticleList());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget buildListArticle() => BlocProvider(
          create: (_) => _articleBloc,
          child: BlocListener<ArticleBloc, ArticleState>(
            listener: (context, state) {
              if (state is ArticleError) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(state.message!),
                  ),
                );
              }
            },
            child: BlocBuilder<ArticleBloc, ArticleState>(
              builder: (context, state) {
                if (state is ArticleInitial) {
                  return const CircularProgressIndicator();
                } else if (state is ArticleLoading) {
                  return const CircularProgressIndicator();
                } else if (state is ArticleLoaded) {
                  return _buildCard(context, state.articleModels);
                } else if (state is ArticleError) {
                  return Container();
                } else {
                  return Container();
                }
              },
            ),
          ),
        );
    return buildListArticle();
  }

  Widget _buildCard(BuildContext context, ArticleModels model) =>
      ListView.builder(
        itemCount: model.data!.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: (() => Get.to(() => ContentArticle(
                  titles: model.data![index].title,
                  desc: model.data![index].content,
                  publish: model.data![index].publishAt,
                  author: model.data![index].user!.name,
                  image: model.data![index].image!.path,
                  categories: model.data![index].category!.first.name,
                ))),
            child: Stack(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 16, right: 16, top: 12, bottom: 12),
                    child: Container(
                      height: defaultMargin * 9,
                      width: defaultMargin * 11.17,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Container(
                              height: 180,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: NetworkImage(
                                        model.data![index].image!.path ?? ""),
                                    fit: BoxFit.fill),
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10, top: 10),
                            child: Text(
                              "${model.data![index].category!.first.name}"
                                  .toUpperCase(),
                              style: primaryLightTextStyle.copyWith(
                                  fontSize: 10, fontWeight: medium),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 10, top: 10, bottom: 10, right: 10),
                            child: Text(
                              "${model.data![index].title}".toUpperCase(),
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: primaryTextStyle.copyWith(
                                  fontWeight: bold, fontSize: 14),
                            ),
                          ),
                          // SingleChildScrollView(
                          //   scrollDirection: Axis.horizontal,
                          //   child: Wrap(
                          //     runSpacing: 1,
                          //     spacing: 1,
                          //     children: model.data!.first.tag!
                          //         .map(
                          //           (postTags) => Padding(
                          //             padding: const EdgeInsets.only(left: 10),
                          //             child: Chip(
                          //               label: Text(
                          //                 "${postTags.name}",
                          //                 style: greenBubbleTextStyle.copyWith(
                          //                     fontSize: 12, fontWeight: medium),
                          //               ),
                          //               backgroundColor: bubbleColor,
                          //             ),
                          //           ),
                          //         )
                          //         .toList(),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );
}
