import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:newslite/Themes/theme.dart';
import 'package:newslite/views/artikel/bloc/article_bloc.dart';
import 'package:newslite/views/artikel/models/article_models.dart';
import 'package:newslite/views/contentarticle/fiturcontentarticle.dart';

part 'artikelList.dart';
part 'article.dart';
