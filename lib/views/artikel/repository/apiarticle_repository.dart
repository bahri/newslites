import 'package:newslite/views/artikel/controller/article_controller.dart';
import 'package:newslite/views/artikel/models/article_models.dart';

class ApiRepositoryArticle {
  final _providerArticle = ApiProviderArticle();

  Future<ArticleModels> fetchArticleList() {
    return _providerArticle.fetchArticleList();
  }
}

class NetworkError extends Error {}
