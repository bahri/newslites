part of 'fiturartikel.dart';

class Article extends StatelessWidget {
  const Article({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Color(0xff003299),
          statusBarIconBrightness: Brightness.light,
        ),
        title: SizedBox(
          width: 200,
          height: 40,
          child: InputDecorator(
            decoration: InputDecoration(
              filled: true,
              fillColor: whiteColor,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              border: const OutlineInputBorder(),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: blackColor, width: 2),
                borderRadius: BorderRadius.circular(15),
              ),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: borderColor),
                  borderRadius: BorderRadius.circular(15)),
            ),
            child: Padding(
              padding: const EdgeInsets.only(right: 36),
              child: InkWell(
                onTap: () => Get.toNamed('/Settings'),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.search,
                      color: primaryColor,
                    ),
                    Text(
                      "Cari Artikel",
                      style: primaryTextStyle.copyWith(
                          fontWeight: medium, fontSize: 12),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        elevation: 0,
        backgroundColor: primaryColor,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  top: 20, left: 25, right: 20, bottom: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Artikel",
                      style: primaryTextStyle.copyWith(
                          fontSize: 24, fontWeight: bold)),
                  // IconButton(
                  //   onPressed: () {},
                  //   icon: Image.asset(
                  //     ImageAssets.filter,
                  //     height: 25,
                  //   ),
                  // ),
                ],
              ),
            ),
            Expanded(
              child: PageView(
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  const ArtikelList(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
