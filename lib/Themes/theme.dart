import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

double defaultMargin = 30.0;
double spacing = 8;

Color primaryColor = const Color(0xff003299);
Color secondaryColor = const Color(0xffFFC801);
Color alertColor = const Color(0xffED6363);
Color priceColor = const Color(0xff2C96F1);
Color bubbleColor = const Color(0xffD0F3EB);
Color backgroundColor1 = const Color(0xff1F1D2B);
Color backgroundColor2 = const Color(0xff2B2937);
Color backgroundColor3 = const Color(0xff242231);
Color backgroundColor4 = const Color(0xff252836);
Color backgroundColor5 = const Color(0xff2B2844);
Color backgroundColor6 = const Color(0xffECEDEF);
Color backgroundColor7 = const Color(0xffE5E5E5);
Color primaryTextColor = const Color(0xffF1F0F2);
Color secondaryTextColor = const Color(0xffE6EBF5);
Color subtitleColor = const Color(0xff504F5E);
Color transparentColor = Colors.transparent;
Color blackColor = const Color(0xff2E2E2E);
Color whiteColor = const Color(0xffFFFFFF);
Color oceanColor = const Color(0xff5BB98B);
Color borderColor = const Color(0xffCCD6EB);
Color textbuttonColor = const Color(0xff665000);
Color buttonColor = const Color(0xff13C59A);
Color yellowColor = const Color(0xffFCAF05);
Color greenColor = const Color(0xff1EA050);
Color greenBubbleColor = const Color(0xff084F3E);
Color primarylightColor = const Color(0xff6684C2);
Color greyColor = const Color(0xff828282);
Color grayColor = const Color(0xff4F4F4F);
Color graysColor = const Color(0xff333333);
TextStyle greenTextStyle = GoogleFonts.ubuntu(
  color: greenColor,
);
TextStyle greenBubbleTextStyle = GoogleFonts.ubuntu(
  color: greenBubbleColor,
);

TextStyle grayTextStyle = GoogleFonts.lato(
  color: grayColor,
);

TextStyle greyTextStyle = GoogleFonts.ubuntu(
  color: greyColor,
);

TextStyle primaryTextStyle = GoogleFonts.ubuntu(
  color: primaryColor,
);

TextStyle primaryLightTextStyle = GoogleFonts.ubuntu(
  color: primarylightColor,
);

TextStyle primaryLTextStyle = GoogleFonts.lato(
  color: primarylightColor,
);

TextStyle orangeTextStyle = GoogleFonts.ubuntu(
  color: yellowColor,
);

TextStyle secondaryTextStyle = GoogleFonts.ubuntu(
  color: secondaryColor,
);

TextStyle subtitleTextStyle = GoogleFonts.ubuntu(
  color: subtitleColor,
);

TextStyle priceTextStyle = GoogleFonts.ubuntu(
  color: priceColor,
);

TextStyle purpleTextStyle = GoogleFonts.ubuntu(
  color: primaryColor,
);

TextStyle blackTextStyle = GoogleFonts.ubuntu(
  color: blackColor,
);

TextStyle blackLatoTextStyle = GoogleFonts.lato(
  color: blackColor,
);

TextStyle graysLatoTextStyle = GoogleFonts.lato(
  color: graysColor,
);

TextStyle whiteTextStyle = GoogleFonts.ubuntu(
  color: whiteColor,
);

TextStyle alertTextStyle = GoogleFonts.ubuntu(
  color: alertColor,
);

TextStyle buttonTextWidgetStyle = GoogleFonts.ubuntu(
  color: textbuttonColor,
);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
